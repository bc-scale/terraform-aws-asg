locals {
  asg_name = "example-dev"
}

resource "aws_key_pair" "this" {
  key_name   = "example"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPycPM8Y0XVJLVrK5gtP42a2YA5iM4uTC0dnzJKPZUjugQsr2f+C8rmw0emtduk1ThprNsEwSjHNDxg3naRxREEZrP1VvTHzCF/2UsWExsbiNinHHtigcrGOVVXVDdF2SL7DOV/UpT2P5DpRG+yqZdpAxSWfGsLC9ENPlB1K8lC1eQiZl5/hCCJqQjyKz6vGFC7vBpjI8kanfuXV1mH/+Aewwx+1jQ7oxBRr8C1PtbFXKj4ZDviKxTdrIXGeLh0xocsfFTApQnoHxdPUbMn+xcTc+N9nBiEaeg8bMwq7NCBZXWaqxMpTDkkuQ5JtBBjAw4Vc7l7/w/9t40Tz3kAFBh rizki@rizkidoank.com"
}

resource "aws_iam_instance_profile" "this" {
  name = "example"
  role = aws_iam_role.this.name
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    effect = "Allow"
  }
}

data "aws_security_group" "default" {
  vpc_id = "vpc-12345"
  name   = "default"
}

resource "aws_iam_role" "this" {
  name               = "example"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

module "asg" {
  source                    = "../../"
  asg_name                  = "example-dev"
  asg_subnet_ids            = ["subnet-12345"]
  environment               = "test"
  asg_image_id              = "ami-08569b978cc4dfa10"
  asg_key_name              = aws_key_pair.this.key_name
  asg_max_size              = 2
  asg_min_size              = 1
  asg_instance_type         = "t3a.nano"
  asg_instance_profile_name = aws_iam_instance_profile.this.name
  asg_security_groups       = [data.aws_security_group.default.id]
}


resource "aws_autoscaling_policy" "scale_out" {
  name                   = format("%s-scale-out", module.asg.asg_name)
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = module.asg.asg_name
}

resource "aws_autoscaling_policy" "scale_in" {
  name                   = format("%s-scale-in", module.asg.asg_name)
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = module.asg.asg_name
}

resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name          = format("%s-cpu-high-alarm", module.asg.asg_name)
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "45"
  alarm_description   = "This metric monitors ec2 cpu utilization for scaling out"
  alarm_actions = [
    aws_autoscaling_policy.scale_out.arn
  ]
  dimensions = {
    AutoScalingGroupName = module.asg.asg_name
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  alarm_name          = format("%s-cpu-low-alarm", module.asg.asg_name)
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "45"
  alarm_description   = "This metric monitors ec2 cpu low utilization for scale in"
  alarm_actions = [
    aws_autoscaling_policy.scale_in.arn
  ]
  dimensions = {
    AutoScalingGroupName = module.asg.asg_name
  }
}